from redbot.core import commands

class HexCog(commands.Cog):
    """Hex <---> Text cog"""
    def __init__(self, bot):
        self.bot = bot
        
    async def hex(self, text):
        pass

    @commands.command()
    async def unhex(self, ctx):
        """Unhexes string, input must be formatted like:
        53 54 55 56"""
        output = ''
        text = ctx.message.content.strip().replace('\n', ' ')
        cache = ''
        for char in text.split(' '):
            try:
                output += chr(int(char, 16))
            except ValueError:
                print("invalid value")
        await ctx.send(output)
    
